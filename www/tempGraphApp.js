myApp = angular.module('app', ['chart.js', 'rzModule'])

myApp.factory('DataFactory', [function () {
    var data = {};

    data.downloadData = function () {
        data.promise = $.get('/data.txt', function (result) {
            console.log("Data Downloaded");
            return result
        }).then(function (result) {
            return $.csv.toObjects(result)
        }).then(function (result) {
            if (typeof data.updateSlider === 'function')
                data.updateSlider(result);
            return result;
        });
        return data.promise;
    }

    data.promise = data.downloadData();

    data.updateData = function () {
        return data.downloadData();
    }
    data.getData = function () {
        return data.promise;
    }

    data.downloadFlights = function () {
        data.flights.promise = $.get('/flights.txt', function (result) {
            console.log("Flights Downloaded");
            return result
        }).then(function (result) {
            return $.csv.toObjects(result)
        }).then(function (result) {
            var flights = {};

            var processFlights = function (i, airlineArray, costArray, dateArray, fieldName) {
                if (result[i][fieldName]) {

                    if (dateArray.length == 0)
                        dateArray.push(moment(result[i].date))
                    else {
                        var timeDiff = dateArray[dateArray.length - 1] - moment(result[i].date)
                        if (timeDiff < -1 * 1000 * 55) {
                            for (var j = 0; j < costArray.length; j++)
                                if (costArray[j].length != dateArray.length)
                                    costArray[j].push(Number.NaN)
                            dateArray.push(moment(result[i].date))
                        }
                    }

                    var airlineIndex = airlineArray.indexOf(data.convertAirline(result[i].airline));
                    if (airlineIndex > -1) {
                        costArray[airlineIndex].push(result[i][fieldName]);
                    }
                    else {
                        airlineArray.push(data.convertAirline(result[i].airline))
                        costArray.push([])
                        for (var j = 0; j < dateArray.length - 1; j++)
                            costArray[costArray.length - 1].push(Number.NaN)
                        costArray[costArray.length - 1].push(result[i][fieldName]);
                    }
                }
            }

            flights.departureAirlines = [];
            flights.returnAirlines = [];
            flights.roundTripAirlines = [];
            flights.departureCosts = [];
            flights.returnCosts = [];
            flights.roundTripCosts = [];
            flights.departureDates = [];
            flights.returnDates = [];
            flights.roundTripDates = [];
            flights.from = result[0].from;
            flights.to = result[0].to;

            for (var i = 0; i < result.length; i++) {
                processFlights(i, flights.departureAirlines, flights.departureCosts, flights.departureDates, 'departureCost')
                processFlights(i, flights.returnAirlines, flights.returnCosts, flights.returnDates, 'returnCost')
                processFlights(i, flights.roundTripAirlines, flights.roundTripCosts, flights.roundTripDates, 'roundTripCostAvg')
            }
            return flights;
        })
        return data.flights.promise;
    }
    data.flights = {}
    data.flights.promise = data.downloadFlights();

    data.updateFlights = function () {
        return data.downloadFlights();
    }
    data.getFlights = function () {
        return data.flights.promise;
    }

    data.setMinDate = function (minDate) {
        data.minDate = minDate;
    }

    data.getMinDate = function () {
        return data.minDate;
    }

    data.setMaxDate = function (maxDate) {
        data.maxDate = maxDate;
    }

    data.registerSliderFunction = function (myFunction) {
        data.updateSlider = myFunction;
    }

    data.findDate = function (date, data, isMin) {
        var min = 0;
        var max = data.length;
        var found = false;
        var index = 0;
        while (!found) {
            var index = Math.floor(min + (max - min) / 2);
            var value = moment(data[index].date);
            if (date < value)
                max = index
            else if (date > value)
                min = index
            else {
                return index
            }

            if (max - min <= 1 && moment(data[min].date) != date && moment(data[max].date) != date) {
                if (isMin)
                    return max
                else
                    return min
            }
        }
    }

    data.processDataForDates = function (originalData) {
        var finalData = [];
        var totalDataPoints = 100;

        if (data.minDate && data.maxDate) {
            var minIndex = data.findDate(data.minDate, originalData, true);
            var maxIndex = data.findDate(data.maxDate, originalData, false);
        } else {
            var minIndex = 0;
            var maxIndex = originalData.length;
        }

        var count = (maxIndex - minIndex) / totalDataPoints;
        if (count < 1)
            count = 1

        for (var i = minIndex; i < maxIndex; i = i + count) {
            finalData.push(originalData[Math.floor(i)]);
        }
        finalData.push(originalData[maxIndex]);
        return finalData;
    }

    data.convertAirline = function (airline) {
        if (airline == 'UA')
            return 'United';
        if (airline == 'AS')
            return 'Alaska';
        if (airline == 'F9')
            return 'Frontier';
        if (airline == 'B6')
            return 'Jet Blue';
        if (airline == 'WS')
            return 'West Jet';
        if (airline == 'VX')
            return 'Virgin';
        if (airline == 'AC')
            return 'Air Canada';
        if (airline == 'swa')
            return 'Southwest';
        return airline
    };

    data.getLowestPrice = function (costArray, airlineArray) {
        var lowestCost = 99999;
        var airline = '';
        var length = 0;
        for (var i = 0; i < costArray.length; i++)
            if (costArray[i].length > length) {
                length = costArray[i].length;
                lowestCost = costArray[i][costArray[i].length - 1]
                airline = airlineArray[i]
            }
            else if (costArray[i][costArray[i].length - 1] < lowestCost) {
                lowestCost = costArray[i][costArray[i].length - 1];
                airline = airlineArray[i];
            }
        return {'cost': lowestCost, 'airline': airline};
    };

    return data;
}]);

myApp.controller("DateCtrl", ['$scope', 'DataFactory',
    function ($scope, DataFactory) {
        $scope.minChanged = false;
        $scope.processData = function (fileData) {
            var min = moment(fileData[0].date);
            var dayPrior = moment(fileData[fileData.length - 1].date).subtract(1, 'day');
            $scope.slider.options.floor = min;
            if (!$scope.minChanged) {
                if (min < dayPrior)
                    $scope.slider.min = dayPrior;
                else
                    $scope.slider.min = min;
            }
            DataFactory.setMinDate($scope.slider.min);

            var max = moment(fileData[fileData.length - 1].date);
            if ($scope.slider.max == $scope.slider.options.ceil)
                $scope.slider.max = max
            $scope.slider.options.ceil = max;
            DataFactory.setMaxDate($scope.slider.max);
        }

        DataFactory.registerSliderFunction($scope.processData);

        $scope.change = function () {
            min = moment($scope.slider.min);
            DataFactory.setMaxDate(moment($scope.slider.max));
            if (!min.isSame(DataFactory.getMinDate())) {
                $scope.minChanged = true;
            }

        }

        DataFactory.getData().done($scope.processData);

        $scope.slider = {
            max: 100,
            options: {
                floor: 0,
                ceil: 100,
                onEnd: $scope.change,
                translate: function (value) {
                    return moment(value).format("D MMM h:mm A");
                }
            }
        };
    }
]);

myApp.controller("TempCtrl", ['$scope', 'DataFactory', '$timeout',

    function ($scope, DataFactory, $timeout) {
        $scope.processData = function (fileData) {
            fileData = DataFactory.processDataForDates(fileData);
            var ADJUSTIR = -0.5; //Amount Temp is off
            var ADJUSTBARO = -1.35
            var ADJUSTHUMID = -0.79
            var labels = [];
            var irAmbData = [];
            var humidAmbData = [];
            var baroAmbData = [];
            var farenheit;
            for (var i = 0; i < fileData.length; i++) {
                labels.push(moment(fileData[i].date));
                farenheit = fileData[i].irAmb * 9 / 5 + 32 + ADJUSTIR;
                irAmbData.push(farenheit);
                //farenheit = fileData[i].humidAmb * 9 / 5 + 32 + ADJUSTHUMID;
                //humidAmbData.push(farenheit);
                farenheit = fileData[i].baroAmb * 9 / 5 + 32 + ADJUSTBARO;
                baroAmbData.push(farenheit);
            }
            $scope.currentTemp = irAmbData[irAmbData.length - 1];
            $scope.labels = labels;
            $scope.series = [
                'Amb Temp From IR',
                //'Amb Temp From Humidity',
                'Amb Temp From Baro'
            ];

            $scope.data = [irAmbData, baroAmbData];
            $timeout(function () {
                value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        }

        $scope.update = function () {
            DataFactory.updateData().done($scope.processData);
        };
        DataFactory.getData().done($scope.processData);

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
            }
        };
    }]);

myApp.controller("BaroCtrl", ['$scope', 'DataFactory', '$timeout',
    function ($scope, DataFactory, $timeout) {

        $scope.processData = function (fileData) {
            fileData = DataFactory.processDataForDates(fileData);
            var labels = [];
            var baroData = [];
            var pressure;
            for (var i = 0; i < fileData.length; i++) {
                labels.push(moment(fileData[i].date));
                pressure = fileData[i].baroHpa / 3386.389 * 100;
                baroData.push(pressure);
            }
            $scope.currentBaro = baroData[baroData.length - 1];
            $scope.labels = labels;
            $scope.data = [];
            $scope.data.push(baroData);
            $timeout(function () {
                value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        };

        $scope.update = function () {
            DataFactory.updateData().done($scope.processData);
        };
        DataFactory.getData().done($scope.processData);

        $scope.series = ['Current Pressure'];

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
                yAxes: [{}]
            }
        };
    }]);

myApp.controller("HumidCtrl", ['$scope', 'DataFactory', '$timeout',
    function ($scope, DataFactory, $timeout) {

        $scope.processData = function (fileData) {
            fileData = DataFactory.processDataForDates(fileData);

            var labels = [];
            var humidData = [];
            for (var i = 0; i < fileData.length; i++) {
                labels.push(moment(fileData[i].date));
                if (fileData[i].humidRel < 99.99)
                    humidData.push(fileData[i].humidRel);
            }
            $scope.currentHumid = humidData[humidData.length - 1];
            $scope.labels = labels;
            $scope.data = []
            $scope.data.push(humidData);
            $timeout(function () {
                var value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        };
        $scope.update = function () {
            DataFactory.updateData().done($scope.processData);
        };
        DataFactory.getData().done($scope.processData);

        $scope.series = ['Relative Humidity'];

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
                yAxes: [{}]
            }
        };
    }]);

myApp.controller("DepartureCtrl", ['$scope', '$timeout', 'DataFactory',
    function ($scope, $timeout, DataFactory) {
        $scope.data = null;
        $scope.csvData = null;

        $scope.processData = function (fileData) {
            $scope.series = fileData.departureAirlines;
            $scope.labels = fileData.departureDates;
            $scope.data = fileData.departureCosts;

            $scope.lowest = DataFactory.getLowestPrice(fileData.departureCosts, fileData.departureAirlines);

            $scope.from = fileData.from;
            $scope.to = fileData.to;

            $timeout(function () {
                var value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        };

        DataFactory.getFlights().done($scope.processData);

        $scope.update = function () {
            //DataFactory.updateData().done($scope.processData);
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.sortData = function (a, b, c) {
            return a.y - b.y;
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 2
                },
                line: {
                    fill: false
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
                yAxes: [{}]
            },
            legend: {
                display: true,
                position: 'left'
            },
            tooltips: {
                itemSort: $scope.sortData
            }
        };
    }]);

myApp.controller("ReturnCtrl", ['$scope', '$timeout', 'DataFactory',
    function ($scope, $timeout, DataFactory) {
        $scope.data = null;
        $scope.csvData = null;

        $scope.processData = function (fileData) {
            $scope.series = fileData.returnAirlines;
            $scope.labels = fileData.returnDates;
            $scope.data = fileData.returnCosts;

            $scope.lowest = DataFactory.getLowestPrice(fileData.returnCosts, fileData.returnAirlines);
            $scope.to = fileData.from;
            $scope.from = fileData.to;

            $timeout(function () {
                var value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        };

        DataFactory.getFlights().done($scope.processData);

        $scope.update = function () {
            //DataFactory.updateData().done($scope.processData);
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.sortData = function (a, b, c) {
            return a.y - b.y;
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 2
                },
                line: {
                    fill: false
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
                yAxes: [{}]
            },
            legend: {
                display: true,
                position: 'left'
            },
            tooltips: {
                itemSort: $scope.sortData
            }
        };
    }]);
myApp.controller("RoundTripCtrl", ['$scope', '$timeout', 'DataFactory',
    function ($scope, $timeout, DataFactory) {
        $scope.data = null;
        $scope.csvData = null;

        $scope.processData = function (fileData) {
            $scope.series = fileData.roundTripAirlines;
            $scope.labels = fileData.roundTripDates;
            $scope.data = fileData.roundTripCosts;

            $scope.lowest = DataFactory.getLowestPrice(fileData.roundTripCosts, fileData.roundTripAirlines);
            $scope.from = fileData.from;
            $scope.to = fileData.to;

            $timeout(function () {
                var value = $scope.data[0].pop;
                $scope.data[0].push(value)
            }, 200);
        };

        DataFactory.getFlights().done($scope.processData);

        $scope.update = function () {
            //DataFactory.updateData().done($scope.processData);
        };

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        $scope.sortData = function (a, b, c) {
            return a.y - b.y;
        };

        $scope.options = {
            elements: {
                point: {
                    radius: 2
                },
                line: {
                    fill: false
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                }],
                yAxes: [{}]
            },
            legend: {
                display: true,
                position: 'left'
            },
            tooltips: {
                itemSort: $scope.sortData
            }
        };
    }]);