import os
import configparser
from datetime import datetime, time
import requests
import json
import time

from sys import argv

from_airport = ""
to_airport = ""
depart_date = ""
return_date = ""
adults = ""
departure_flight_depart_earliest = "00:00"
departure_flight_depart_latest = "23:59"
return_flight_depart_earliest = "00:00"
return_flight_depart_latest = "23:59"

try:
    import config
except IOError:
    print("Please create config.py and place google_api_key = YOUR_KEY")

try:
    doc = open("/home/eric/ticketscraper/flights.txt", 'a')
    if os.stat("/home/eric/ticketscraper/flights.txt").st_size == 0:
        doc.write("date,from,to,departureCost,returnCost,roundTripCostAvg,airline\n")
        print("New file created")

except IOError as e:
    doc = open("flights.txt", 'a')
    if os.stat("flights.txt").st_size == 0:
        doc.write("date,from,to,departureCost,returnCost,roundTripCostAvg,airline\n")
        print("New file created")


def write(s):
    doc.write(s)
    doc.flush()


for i in range(0, len(argv)):
    if '-from' == argv[i]:
        from_airport = argv[i + 1]
    if '-to' == argv[i]:
        to_airport = argv[i + 1]
    if '-depart' == argv[i]:
        depart_date = argv[i + 1]
    if '-return' == argv[i]:
        return_date = argv[i + 1]
    if '-adults' == argv[i]:
        adults = argv[i + 1]
    if '-departure_earliest' == argv[i]:
        departure_flight_depart_earliest = argv[i + 1]
    if '-departure_latest' == argv[i]:
        departure_flight_depart_latest = argv[i + 1]
    if '-return_earliest' == argv[i]:
        return_flight_depart_earliest = argv[i + 1]
    if '-return_latest' == argv[i]:
        return_flight_depart_latest = argv[i + 1]


def get_google_flights(from_airport=from_airport, to_airport=to_airport, adults=adults, depart_date=depart_date,
                       return_date=return_date, departure_flight_depart_earliest=departure_flight_depart_earliest,
                       departure_flight_depart_latest=departure_flight_depart_latest,
                       return_flight_depart_earliest=return_flight_depart_earliest,
                       return_flight_depart_latest=return_flight_depart_latest,
                       round_trip=True):
    google_URL = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=' + config.google_api_key
    headers = {'content-type': 'application/json'}

    payload = {
        "request": {
            "passengers": {
                "kind": "qpxexpress#passengerCounts",
                "adultCount": adults,
                "childCount": '0',
                "infantInLapCount": '0',
                "infantInSeatCount": '0',
                "seniorCount": '0'
            },
            "slice": [
                {
                    "kind": "qpxexpress#sliceInput",
                    "origin": from_airport,
                    "destination": to_airport,
                    "date": depart_date,
                    # "maxStops": integer,
                    # "maxConnectionDuration": integer,
                    # "preferredCabin": string,
                    "permittedDepartureTime": {
                        "kind": "qpxexpress#timeOfDayRange",
                        "earliestTime": departure_flight_depart_earliest,
                        "latestTime": departure_flight_depart_latest
                    },
                    # "permittedCarrier": [
                    #  string
                    # ],
                    # "alliance": string,
                    # "prohibitedCarrier": [
                    #  string
                    # ]
                },
            ],
            # "maxPrice": string,
            "saleCountry": 'US',
            "ticketingCountry": 'US',
            # "refundable": boolean,
            "solutions": 50
        }
    }

    if round_trip:
        payload['request']['slice'].append({
            "kind": "qpxexpress#sliceInput",
            "origin": to_airport,
            "destination": from_airport,
            "date": return_date,
            "permittedDepartureTime": {
                "kind": "qpxexpress#timeOfDayRange",
                "earliestTime": return_flight_depart_earliest,
                "latestTime": return_flight_depart_latest
            },
        })

    response = requests.post(google_URL, data=json.dumps(payload), headers=headers)
    return response.json()


def write_file_line(carrier, from_airport=from_airport, to_airport=to_airport,
                    departure_cost="", return_cost="", round_trip_avg=""):
    write(datetime.now().isoformat() +
          ',' + from_airport +
          ',' + to_airport +
          ',' + departure_cost +
          ',' + return_cost +
          ',' + round_trip_avg +
          ',' + carrier +
          '\n')


while True:
    result = get_google_flights()
    if 'trips' in result:
        trips = result['trips']
        carriers = []
        prices = []

        for trip in trips['tripOption']:
            price = float(trip['pricing'][0]['saleTotal'][3:])
            carrier = trip['slice'][0]['segment'][0]['flight']['carrier']
            if carrier not in carriers:
                carriers.append(carrier)
                prices.append(price)

        for i in range(0, len(carriers)):
            write_file_line(round_trip_avg=str(prices[i] / 2), carrier=carriers[i])

    result = get_google_flights(round_trip=False)
    if 'trips' in result:
        trips = result['trips']
        carriers = []
        prices = []

        for trip in trips['tripOption']:
            price = float(trip['pricing'][0]['saleTotal'][3:])
            carrier = trip['slice'][0]['segment'][0]['flight']['carrier']
            if carrier not in carriers:
                carriers.append(carrier)
                prices.append(price)

        for i in range(0, len(carriers)):
            write_file_line(carrier=carriers[i], departure_cost=str(prices[i]))

    result = get_google_flights(depart_date=return_date, from_airport=to_airport,
                                to_airport=from_airport, round_trip=False,
                                departure_flight_depart_earliest=return_flight_depart_earliest,
                                departure_flight_depart_latest=return_flight_depart_latest)
    if 'trips' in result:
        trips = result['trips']
        carriers = []
        prices = []

        for trip in trips['tripOption']:
            price = float(trip['pricing'][0]['saleTotal'][3:])
            carrier = trip['slice'][0]['segment'][0]['flight']['carrier']
            if carrier not in carriers:
                carriers.append(carrier)
                prices.append(price)

        for i in range(0, len(carriers)):
            write_file_line(carrier=carriers[i], return_cost=str(prices[i]))

    southwest_config = 'https://mobile.southwest.com/js/config.js'
    response = requests.get(southwest_config)

    part = response.text.split('API_KEY:"')
    api_key = part[1].split('"')[0]

    southwest_url = ('https://mobile.southwest.com/api/extensions/v1/mobile/flights/products/'
                     '?origination-airport=' + from_airport +
                     '&destination-airport=' + to_airport +
                     '&departure-date=' + depart_date +
                     '&departure-date2=' + return_date +
                     '&number-adult-passengers=' + adults +
                     '&number-senior-passengers=0' +
                     '&currency-type=Dollars')

    sw_headers = {
        'Referer': 'https://mobile.southwest.com/',
        'X-API-Key': api_key,
        'X-Requested-With': 'XMLHttpRequest'
    }

    response = requests.get(url=southwest_url, headers=sw_headers)

    lowest_departure_cost = 99999
    lowest_return_cost = 99999
    json_result = response.json()
    trips = json_result['trips']
    departure_flight_depart_earliest_conv = datetime.strptime(departure_flight_depart_earliest, '%H:%M').time()
    departure_flight_depart_latest_conv = datetime.strptime(departure_flight_depart_latest, '%H:%M').time()
    return_flight_depart_earliest_conv = datetime.strptime(return_flight_depart_earliest, '%H:%M').time()
    return_flight_depart_latest_conv = datetime.strptime(return_flight_depart_latest, '%H:%M').time()

    if len(trips) > 0:
        for segment in trips[0]['airProducts']:
            segment_time = segment['segments'][0]['departureDateTime']
            segment_time_conv = datetime.strptime(segment_time, '%Y-%m-%dT%H:%M').time()
            if segment_time_conv > departure_flight_depart_earliest_conv and segment_time_conv < departure_flight_depart_latest_conv:
                for price in segment['fareProducts']:
                    this_price = price['currencyPrice']['discountedTotalFareCents'] / 100
                    if this_price < lowest_departure_cost:
                        lowest_departure_cost = this_price
    if len(trips) > 1:
        for segment in trips[1]['airProducts']:
            segment_time = segment['segments'][1]['departureDateTime']
            segment_time_conv = datetime.strptime(segment_time, '%Y-%m-%dT%H:%M').time()
            if segment_time_conv > return_flight_depart_earliest_conv and segment_time_conv < return_flight_depart_latest_conv:
                for price in segment['fareProducts']:
                    this_price = price['currencyPrice']['discountedTotalFareCents'] / 100
                    if this_price < lowest_return_cost:
                        lowest_return_cost = this_price

    write_file_line(carrier='swa', from_airport=from_airport, to_airport=to_airport,
                    departure_cost=str(lowest_departure_cost),
                    return_cost=str(lowest_return_cost))

    time.sleep(60 * 60 * 2)
