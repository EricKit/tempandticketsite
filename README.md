# README #

### Purpose ###

There are two completely separate projects linked by a website. One reads sensor data from a Bluetooth TI SensorTag. The other queries Google Flights with an API key and Southwest Mobile. These each produce data which is displayed on a website. This website, in my case, is hosted on a raspberry pi and connected to a monitor in the house to show the information.

* Just for personal use

### Files ###

* www folder is my website
* data.txt is the TI sensor data, flights.txt is the flight data.
* flights.txt and data.txt should be symbolic links from the www folder to the actual data.
* runMonitor and runTickets files are bash scripts that make it run on start
* monitor.py handles the sensor suite
* tickets.py handles the tickets

SensorTag screenshot:
![Demo Picture](demoPic1.jpeg?raw=true)

Flight section screenshot:
![Demo Picture](demoPic2.jpeg?raw=true)